# food_laboratory

User enters a list of available products and web-service gives a list of dishes that he could cook from them.  
The user can also collaborate with another user and prepare a dish from the products available to both of them.  
Web-service owners can add new recipes to the web service database.